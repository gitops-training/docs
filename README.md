# DOCS

**Gitops Traning** é um projeto criado para explorar as possíbilidades que o **git** trás, pondo em prática o conceito de **gitops**, gerenciando configurações de infraestrutura e aplicações com um unico sistema de controle de versão. Para mais informações sobre o que é *gitops* recomendamos o artigo [O que é o GitOps?](https://www.redhat.com/pt-br/topics/devops/what-is-gitops) da [RedHat](https://www.redhat.com/en).

Esse conjunto de projetos tem como objetivo ser uma referência prática para quem deseja aprender ou aperfeiçoar seus conhecimentos, sempre buscando manter atualizada as informações e utilizar novas tecnologias que virão.

Nesse projeto se encontra toda documentação dos projetos, explicando os por menores de cada um e como replicar e adapta-lo para o seu cenário.

Sugestões, correções e melhorias são sempre bem vindos!

